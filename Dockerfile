FROM python:3.5-alpine
ADD . /var/www/project/ticket_api_test
WORKDIR /var/www/project/ticket_api_test
RUN pip install -r requirements.txt
CMD ["python", "app.py"]