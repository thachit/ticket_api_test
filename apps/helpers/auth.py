# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import datetime

from werkzeug.security import check_password_hash

from apps.models.user import User as UserModel


def process_login(username, password):

    user_obj = UserModel.objects.get(username=username)

    if user_obj:
        if check_password_hash(user_obj.password, password) and user_obj.active:
            user_obj.last_login = datetime.datetime.now()
            user_obj.save()
            return True

    return False



