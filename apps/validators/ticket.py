# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

from marshmallow import Schema, fields
from marshmallow.validate import Length


class Ticket(Schema):
    subject = fields.Str(required=True, validate=Length(min=1, max=200))
    content = fields.Str(required=True, validate=Length(min=1))

class Reply(Schema):
    content = fields.Str(required=True, validate=Length(min=1))