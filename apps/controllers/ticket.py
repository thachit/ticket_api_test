# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import json
from flask import Blueprint, request, make_response

from apps.utilities.jwt_token import token_required
from apps.models.ticket import Ticket as TicketModel
from apps.models.ticket import Reply as ReplyModel
from apps.models.user import User as UserModel
from apps.validators.ticket import Ticket as TicketSchema
from apps.validators.ticket import Reply as ReplySchema

ticket = Blueprint('ticket', __name__)

@ticket.route('/api/v1/ticket/get', methods=['GET'])
@token_required
def get():
    ticket_obj = TicketModel.objects()
    response = make_response(ticket_obj.to_json())
    response.status_code = 200
    response.content_type = 'application/json; charset=utf-8'
    return response


@ticket.route('/api/v1/ticket/get/<ticket_id>', methods=['GET'])
@token_required
def get_by_id(ticket_id):
    ticket_obj = TicketModel.objects(id=ticket_id)
    response = make_response(ticket_obj.to_json())
    response.status_code = 200
    response.content_type = 'application/json; charset=utf-8'
    return response


@ticket.route('/api/v1/ticket/create', methods=['POST'])
@token_required
def create():

    username = request.username
    user_obj = UserModel.objects.get(username=username)

    if user_obj:

        form_data = request.json
        create_ticket_schema = TicketSchema()
        errors = create_ticket_schema.validate(form_data)

        if errors:
            response = make_response(json.dumps({'error': errors}))
            response.status_code = 500

        else:
            ticket_obj = TicketModel(
                subject=form_data.get('subject'),
                content=form_data.get('content'),
                create_by=user_obj)

            ticket_obj.save()

            response = make_response(ticket_obj.to_json())
            response.status_code = 201

    else:
        response = make_response(json.dumps({'error': 'Could not find User'}))
        response.status_code = 403

    response.content_type = 'application/json; charset=utf-8'
    return response


@ticket.route('/api/v1/ticket/reply/create/<ticket_id>', methods=['PUT'])
@token_required
def reply(ticket_id):

    ticket_obj = TicketModel.objects.get(id=ticket_id)

    if ticket_obj:
        username = request.username
        user_obj = UserModel.objects.get(username=username)

        form_data = request.json

        reply_schema = ReplySchema()
        errors = reply_schema.validate(form_data)

        if errors:
            response = make_response(json.dumps({'error': errors}))
            response.status_code = 500
        else:

            reply_obj = ReplyModel(
                content=form_data.get('content'),
                create_by=user_obj
            )

            ticket_obj.comments.append(reply_obj)
            ticket_obj.save()

            response = make_response(ticket_obj.to_json())
            response.status_code = 201
    else:
        response = make_response(json.dumps({'error': "Ticket not Found"}))
        response.status_code = 404
    response.content_type = 'application/json; charset=utf-8'
    return response


@ticket.route('/api/v1/ticket/reply/delete/<ticket_id>', methods=['PUT'])
@token_required
def delete_reply(ticket_id):

    ticket_obj = TicketModel.objects(id=ticket_id)
    if ticket_obj:
        username = request.username
        user_obj = UserModel.objects.get(username=username)

        form_data = request.json

        reply_id = form_data.get("reply_id")

        for comment in ticket_obj[0].comments:
            if str(comment.oid) == reply_id:
                if str(comment.create_by.id) == str(user_obj.id):
                    ticket_obj.update_one(pull__comments__oid=reply_id)

        response = make_response(ticket_obj.to_json())
        response.status_code = 201

    else:
        response = make_response(json.dumps({'error': "Ticket not found!"}))
        response.status_code = 404

    response.content_type = 'application/json; charset=utf-8'
    return response


# a = ReplyModel.objects(oid="5e6b0b22b6d75e5c089fd266")

# a = TicketModel.objects(comments__oid = "5e6b0eafb6d75e5d8ab573f6")

# ticket_obj.update_one(
#     pull__comments__oid="5e6b0b22b6d75e5c089fd266"l
# )
