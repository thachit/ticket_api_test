# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import datetime
from bson.objectid import ObjectId
from mongoengine import Document, EmbeddedDocument, StringField, ReferenceField, DateTimeField, EmbeddedDocumentField, \
    SortedListField, ObjectIdField
from .user import User


class Reply(EmbeddedDocument):
    oid = ObjectIdField(required=True, default=ObjectId,
                        unique=True, primary_key=True)
    content = StringField(required=True)
    created_date = DateTimeField(default=datetime.datetime.now())
    create_by = ReferenceField(User, required=True)


class Ticket(Document):
    STATES = (
        ('new', 'New'),
        ('in_progress', 'In Progress'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled')
    )

    subject = StringField(max_length=200, required=True)
    content = StringField(required=True)
    created_date = DateTimeField(default=datetime.datetime.now())
    create_by = ReferenceField(User, required=True)
    comments = SortedListField(EmbeddedDocumentField(Reply), ordering="created_date", reverse=True)
    # status = StringField(max_length=50, choices=STATES, default='new')
