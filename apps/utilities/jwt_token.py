# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import jwt
from jwt.exceptions import *
import datetime
import json

from flask import make_response, request
from functools import wraps
from flask import request


__SECRECT_KEY = 'XllbkBua2@lkZ3J'


def create_jwt(username):

    message = {
        'username': username,
        'iss': 'nguyencothach1989@gmail.com',
        'iat': datetime.datetime.utcnow(),
        'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24),
    }

    return jwt.encode(message, __SECRECT_KEY, algorithm='HS256').decode('utf-8')


def decode_jwt(token):
    try:
        payload = jwt.decode(token, __SECRECT_KEY, algorithms=['HS256'])
        return payload
    except (InvalidSignatureError, ExpiredSignature, InvalidIssuerError, DecodeError) as exc:
        return None


def token_required(func):
    '''
    :param func: The view function to decorate.
    :type func: function
    '''

    @wraps(func)
    def decorated_view(*args, **kwargs):

        authorization = request.headers.get('Authorization', None)

        payload = decode_jwt(authorization)
        if payload:
            request.username = payload.get('username')
        else:
            data_respone = json.dumps({'message': 'not authorization'})
            status_code = 401
            data_respone = data_respone
            response = make_response(data_respone)
            response.status_code = status_code
            response.content_type = 'application/json; charset=utf-8'
            return response

        return func(*args, **kwargs)

    return decorated_view

