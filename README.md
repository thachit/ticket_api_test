# Ticket API Test

## Requirements

- python == 3.5
- mongodb == 3.0.9
- docker and docker-compose (optional if you want to deploy in Docker)

The App will be run on port 8080 and 27017 (mongodb). Please make sure these ports is available,

## ENVs

you should set up your environment vars:

[ENVs]: #env

- `DB_HOST=127.0.0.1`
- `DB_PORT=27017` 
- `DB_NAME=ticket_api_app`

## Local development

Create a virtual environment using your favorite manager and install backend dependencies.
Development dependencies are listed in **`requirements.txt`**.

- `pyvenv venv`
- `source ./venv/bin/activate`
- `pip install -r requirements.txt`

Run API Server

- `python app.py`

Navigate to

- `http://127.0.0.1:8080/`

## Docker Deployment

If you want to run in Docker, type command:

- `docker-compose up`

OR, run in background

- `docker-compose up -d`

## APIs

1. Sign Up: 
    - `/api/v1/user/create`
2. Login: 
    - `/api/v1/user/login`
3. Create Ticket *(Authorization Required)*: 
    - `/api/v1/ticket/create`
4. Get All Ticket *(Authorization Required)*:
    - `/api/v1/ticket/get`
5. Get Ticket By ID *(Authorization Required)*:
    - `/api/v1/ticket/get/<TicketID>`
6. Reply Ticket *(Authorization Required)*:
    - `/api/v1/ticket/reply/create/<TicketID>`
7. Remove Reply *(Authorization Required)*:
    - `/api/v1/ticket/reply/delete/<TicketID>`
    
## Consume API using POSTMAN 
- Import `./postman/ticket_api_test.postman_collection.json` to Postman Collections
- Import `./postmans/Ticket API Test.postman_environment.json` to Postman Environments
    - Changing host, port parameters to your local server in environments
    - After run API login, add returned JWT Token to token parameter in environment