# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import pkgutil
import sys
import json
import os
from mongoengine import connect
from flask import Flask, make_response


class FlaskHttp:

    __app = None

    def __init__(self):

        self.DATABASE_HOST = os.environ.get('DB_HOST', '127.0.0.1')
        self.DATABASE_PORT = int(os.environ.get('DB_PORT', 27017))
        self.DATABASE_NAME = os.environ.get('DB_NAME', 'ticket_api_app')
        self.USERNAME = ''
        self.PASSWORD = ''
        self.AUTH_SOURCE = ''

        self.__app = Flask(__name__)

        # Database connection
        self.database_connection()

        # Load Controller Module
        EXTENSIONS_DIR = "apps/controllers"
        EXTENSIONS_MODULE = "apps.controllers"
        modules = pkgutil.iter_modules(path=[EXTENSIONS_DIR])
        for loader, mod_name, ispkg in modules:
            if mod_name not in sys.modules:
                # It imports fine
                loaded_mod = __import__(EXTENSIONS_MODULE+"."+mod_name, fromlist=[mod_name])
                blueprint_obj = getattr(loaded_mod, mod_name)

                # It does not register
                self.__app.register_blueprint(blueprint_obj)

        # Customize Error page
        @self.__app.errorhandler(401)
        def custom_401(error):
            response = make_response(json.dumps({'error': error}))
            response.status_code = 401
            response.content_type = 'application/json; charset=utf-8'
            return response

        @self.__app.errorhandler(403)
        def custom_403(error):
            response = make_response(json.dumps({'error': error.description}))
            response.status_code = error.code
            response.content_type = 'application/json; charset=utf-8'
            return response

        @self.__app.errorhandler(404)
        def custom_error(error):
            response = make_response(json.dumps({'error': error.description}))
            response.status_code = error.code
            response.content_type = 'application/json; charset=utf-8'
            return response

        @self.__app.errorhandler(500)
        def custom_500(error):
            response = make_response(json.dumps({'error': error.args[0]}))
            response.status_code = 500
            response.content_type = 'application/json; charset=utf-8'
            return response

        @self.__app.errorhandler(Exception)
        def handle_exception(error):
            # now you're handling non-HTTP exceptions only
            response = make_response(json.dumps({'error': error.args[0]}))
            response.status_code = 500
            response.content_type = 'application/json; charset=utf-8'
            return response

    # Database configuration
    def database_connection(self):
        print("Connect to database from Host {}".format(self.DATABASE_HOST))
        connect(self.DATABASE_NAME, host=self.DATABASE_HOST, port=self.DATABASE_PORT)

    def run(self):
        print("START APPLICATION WITH HOST 0.0.0.0 AND PORT 8080")
        self.__app.run(host='0.0.0.0', port=8080, debug=False)


if __name__ == '__main__':

    flask_http = FlaskHttp()
    flask_http.run()

